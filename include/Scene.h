#pragma once

#include <vector>
#include <assert.h>
#include <fstream>

#include "Primitive.h"
#include "MathLib.h"

typedef unsigned int uint;

#define REFLECTION_BIAS 1e-5
#define REFRACTION_BIAS 1e-4
#define SHADOW_BIAS 1e-4

#define MSAA_ENABLED 0

#define SAFE_DELETE(x) { if (x) { delete x; x  = nullptr; }}
#define SAFE_DELETE_ARRAY(x) { if (x) { delete[] x; x = nullptr; }}

#define GAMMA_CORRECTION 2.2f

typedef struct
{
	int width;
	int height;
	int fov;
	int numSamples;
	int maxDepth;
	Vec3 backgroundCol = Vec3(.68f, .78f, .95f);
}RenderParams;

class Scene
{
public:
	Scene(const RenderParams& params) : m_params(params)
	{
		// Allocate a frame buffer of sizeof(width * height)
		m_frameBuffer = new Vec3[m_params.width * m_params.height];
	};

	~Scene()
	{
		SAFE_DELETE_ARRAY(m_frameBuffer);

		for (const Primitive* obj : m_objects)
		{
			SAFE_DELETE(obj);
		}
		m_objects.clear();

		for (const Primitive* l : m_lights)
		{
			SAFE_DELETE(l);
		}
		m_lights.clear();
	}

	void addObject(Primitive* obj) { m_objects.push_back(obj); }
	void addLight(Primitive* obj) { m_lights.push_back(obj); }

	const std::vector<Primitive*>& getObjects() const { return m_objects; }
	const std::vector<Primitive*>& getLights() const { return m_lights; }

	/** Calculate primary rays and shoot from the center of pixels */
	void render()
	{
		float aspectratio = (float)m_params.width / m_params.height;
		float fovY = tan(M_PI * 0.5f * m_params.fov / 180.f);
		const Vec3 origin = 0;
#pragma omp parallel for schedule(dynamic, 1)
		for (int y = 0; y < m_params.height; ++y)
		{
			for (int x = 0; x < m_params.width; ++x)
			{
#if MSAA_ENABLED
				Vec3 col = 0;
				for (int sy = 0; sy < m_params.numSamples; sy++)
				{
					for (int sx = 0; sx < m_params.numSamples; sx++)
					{
						// Calculate pixel center, convert to NDC taking into account aspect ratio & FOVy
						float dx = (2 * ((x + 0.5 * sx / m_params.numSamples) / m_params.width) - 1) * fovY * aspectratio;
						float dy = (1 - 2 * ((y + 0.5 * sy / m_params.numSamples) / m_params.height)) * fovY;
						Vec3 dir = normalize(Vec3(dx, dy, -1));
						float pseudo = 0.f;
						col += shade(Ray(origin, dir), 0, pseudo);
					}
				}

				m_frameBuffer[y * m_params.width + x] = col / (m_params.numSamples * m_params.numSamples);
#else
				// Calculate pixel center, convert to NDC taking into account aspect ratio & FOVy
				float dx = (2 * ((x + 0.5) / m_params.width) - 1) * fovY * aspectratio;
				float dy = (1 - 2 * ((y + 0.5) / m_params.height)) * fovY;
				Vec3 dir = normalize(Vec3(dx, dy, -1));
				Vec3 col = shade(Ray(origin, dir), 0);
				m_frameBuffer[y * m_params.width + x] = col;
#endif
			}
		}
	}

	Primitive * findNearest(const Ray & ray, float& dist)
	{
		Primitive* prim = nullptr;
		for (int i = 0; i < m_objects.size(); i++)
		{
			float t = INFINITY;
			if (m_objects[i]->intersects(ray, t) == RayResult::HIT)
			{
				if (t < dist)
				{
					dist = t;
					prim = m_objects[i];
				}
			}
		}

		return prim;
	}

	Vec3 shade(const Ray& ray, int depth)
	{
		if (depth > m_params.maxDepth) return m_params.backgroundCol;

		float dist = INFINITY;
		Primitive* prim = findNearest(ray, dist);
		if (!prim) return m_params.backgroundCol; // If ray intersected nothing, simply draw BACK_COL

		Vec3 surfaceCol = 0; // Surface color at hit point
		Vec3 hit = ray.m_origin + ray.m_direction * dist; // Point where ray hit the closest object
		Vec3 un = normalize(prim->getSurfaceNormal(hit)); // Normal at intersection
		bool inout = false;
		Vec3 n = dot(un, ray.m_direction) > 0 ? -un, inout = true : un; // Handle orientation

		const Material& mat = prim->getMaterial();

		// Shoot a reflection ray if object is reflective and current recursion depth is not exceeded
		if (mat.getType() == MaterialType::REFL || mat.getType() == MaterialType::REFL_AND_REFR)
		{
			// Compute reflection
			Vec3 r = reflect(ray.m_direction, n);
			Vec3 reflCol = shade(Ray(hit + r * REFLECTION_BIAS, r), depth + 1);

			Vec3 refrCol = 0;
			if (mat.getType() == MaterialType::REFL_AND_REFR) // Compute refraction
			{
				assert(mat.getTransparency() > 0.f && "Adjust object transparency for refraction!");

				float inIOR = 1.f / mat.getIOR();
				float cosI = dot(-n, ray.m_direction);
				float cosT = 1.0f - inIOR * inIOR * (1.0f - cosI * cosI);
				Vec3 T = (ray.m_direction * inIOR) + n * (inIOR * cosI - sqrtf(cosT));
				refrCol = shade(Ray(hit + T * REFRACTION_BIAS, T), depth + 1);
				Vec3 abb = mat.getSolidColor() * .25;
				Vec3 trans = Vec3(expf(abb.r), expf(abb.g), expf(abb.b));
				refrCol *= trans;
			}

			surfaceCol = (reflCol * mat.getReflection() + refrCol * mat.getTransparency()) * mat.getSolidColor();
		}
		else
		{
			for (const Primitive* obj : m_lights)
			{
				Vec3 l = normalize(obj->getPosition() - hit);
				float inShadow = 1.0f; // Whether object is occluded
				for (const Primitive* s : m_objects)
				{
					float t = INFINITY;
					Ray shadowRay(hit + n * SHADOW_BIAS, l); // Add some bias to shadow ray
					if (s->intersects(shadowRay, t) == RayResult::HIT)
					{
						// Object occluded by another object
						inShadow = 0.f;
						break;
					}
				}

				// Compute diffuse shading
				float ndotl = fmaxf(0.f, dot(n, l));
				surfaceCol += ((mat.getSolidColor() * obj->getMaterial().getLightIntensity()
					* obj->getMaterial().getSolidColor()) * (inShadow * ndotl)) / M_PI;

				// Compute specular shading if object has specular component and diffuse lighting is present
				if (length2(mat.getSpecular()) > 0.f && ndotl > 0.f)
				{
					Vec3 r = reflect(l, n);
					surfaceCol += (mat.getSolidColor() * obj->getMaterial().getSolidColor() * mat.getSpecular() * obj->getMaterial().getLightIntensity())
						* (inShadow * powf(fmaxf(0.f, dot(ray.m_direction, r)), mat.getShininess()));
				}
			}
		}

		return surfaceCol;
	}

	/** Export frame buffer content to .ppm file */
	void export()
	{
		std::ofstream render("./render.ppm", std::ios::out | std::ios::binary);
		render << "P6\n" << m_params.width << " " << m_params.height << "\n255\n";
		for (uint i = 0; i < m_params.width * m_params.height; i++)
		{
			const Vec3& col = m_frameBuffer[i];
			render << (uint8_t)readjustOutput(col.x) << (uint8_t)readjustOutput(col.y) << (uint8_t)readjustOutput(col.z);
		}

		render.close();
	}

private:
	Vec3* m_frameBuffer = nullptr;

	RenderParams m_params;

	std::vector<Primitive*> m_objects;
	std::vector<Primitive*> m_lights;

	/** Clamp color value and apply gamma-correction */
	int readjustOutput(const float val)
	{
		float res = powf(fminf(1.f, val), 1 / GAMMA_CORRECTION);
		res *= 255;

		return (int)res;
	}
};