#pragma once

#include "MathLib.h"

class Ray
{
public:
	Ray(const Vec3& origin, const Vec3& dir) : m_origin(origin), m_direction(dir) {}
	~Ray() {}

	Vec3 m_origin, m_direction;
};

// 3 types of materials: Diffuse [solid], Reflective [Mirror], Reflective & Reflective [Glass]
enum class MaterialType { DIFF, REFL, REFL_AND_REFR, INVALID };

class Material
{
public:
	Material(const Vec3& solid, MaterialType type) : m_type(type), m_solidColor(solid) {}
	~Material() {}

	const Vec3& getSolidColor() const { return m_solidColor; }
	const Vec3& getSpecular() const { return m_specular; }
	const Vec3& getLightIntensity() const { return m_intensity; }

	MaterialType getType() const { return m_type; }

	float getShininess() const { return m_shininess; }
	float getReflection() const { return m_reflection; }
	float getIOR() const { return m_ior; }
	float getTransparency() const { return m_transparent; }

	void setSpecular(const Vec3& val) { m_specular = val; }
	void setLightIntensity(const Vec3& val) { m_intensity = val; }

	void setShininess(float val) { m_shininess = val; }
	void setReflection(float val) { m_reflection = val; }
	void setTransparent(float val) { m_transparent = val; }
	void setIOR(float val) { m_ior = val; }

private:
	Vec3 m_solidColor; // Albedo
	Vec3 m_specular; // For specular highlights
	Vec3 m_intensity; // For lights

	float m_shininess = 0.f; // For specular highlights
	float m_reflection = 0.f; // Reflectiveness
	float m_transparent = 0.f; // Transparency
	float m_ior = 1.0f;

	MaterialType m_type = MaterialType::DIFF;

	// Prevent copy
	Material(const Material& self) {}
};

enum class RayResult { HIT, MISS };

class Primitive
{
public:
	Primitive(const Vec3& pos, const Vec3& solid, MaterialType type)
		: m_material(solid, type), m_position(pos)
	{}

	virtual ~Primitive() {}

	const Vec3& getPosition() const { return m_position; }

	virtual RayResult intersects(const Ray& ray, float& t) const = 0;
	virtual Vec3 getSurfaceNormal(const Vec3& point = Vec3(0)) const = 0;

	Material& getMaterial() { return m_material; }
	const Material& getMaterial() const { return m_material; }

protected:
	Vec3 m_position;

	Material m_material;
};

enum class ShadowType { HARD, SOFT };

class Sphere : public Primitive
{
public:
	Sphere(const Vec3 &cent, float rad, const Vec3& sc, MaterialType type)
		: Primitive(cent, sc, type), m_radius(rad)
	{
	}

	~Sphere() {}

	virtual RayResult intersects(const Ray& ray, float& t) const override
	{
		Vec3 v = m_position - ray.m_origin;
		float b = dot(v, ray.m_direction);
		if (b < 0) return RayResult::MISS;

		float d2 = dot(v, v) - b * b;
		if (d2 > m_radius * m_radius) return RayResult::MISS;

		float det = sqrt(m_radius * m_radius - d2);
		float t0 = b - det;
		float t1 = b + det;

		if (t0 < 0) t0 = t1;
		t = t0;

		return RayResult::HIT;
	}

	virtual Vec3 getSurfaceNormal(const Vec3& hit = Vec3(0)) const override
	{
		return hit - m_position;
	}

	float m_radius;
};

class Plane : public Primitive
{
public:
	Plane(const Vec3 &n, float d, const Vec3& sc, MaterialType type)
		: Primitive(n, sc, type), m_d(d)
	{
	}

	~Plane() {}

	virtual RayResult intersects(const Ray & ray, float & t) const override
	{
		float l = dot(ray.m_direction, m_position);
		if (!(l <= EPSILON && l > EPSILON))
		{
			float dist = -(dot(m_position, ray.m_origin) + m_d) / l;
			if (dist > 0)
			{
				t = dist;

				return RayResult::HIT;
			}
		}

		return RayResult::MISS;
	}

	virtual Vec3 getSurfaceNormal(const Vec3 & point = Vec3(0)) const override
	{
		return m_position;
	}

private:
	float m_d;
};