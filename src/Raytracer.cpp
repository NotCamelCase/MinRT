#include <iostream>
#include <chrono>

#include "Scene.h"
#include "Primitive.h"
#include "MathLib.h"

using namespace std::chrono;

int main(int argc, char* argv[])
{
	srand(time(NULL));

	printf("Setting scene...\n");

	// Render setup
	RenderParams g_params;
	g_params.width = 1920;
	g_params.height = 1080;
	g_params.fov = 45;
	g_params.maxDepth = 10;
	g_params.numSamples = 4;
	g_params.backgroundCol = Vec3(.68f, .718f, .95f);

	Scene rt(g_params);

	Sphere* s1 = new Sphere(Vec3(-0.75, -.25, -2.5), .5f, Vec3(0.25, 0.5, 0.75), MaterialType::DIFF);
	s1->getMaterial().setShininess(96.f);
	s1->getMaterial().setSpecular(Vec3(.75f));
	rt.addObject(s1);

	Sphere* s2 = new Sphere(Vec3(0.5, 0., -4.5), .6f, Vec3(0.13, 0.71, 0.), MaterialType::REFL);
	s2->getMaterial().setShininess(100.f);
	s2->getMaterial().setSpecular(.5f);
	s2->getMaterial().setReflection(0.75f);
	rt.addObject(s2);

	Sphere* s3 = new Sphere(Vec3(0.5, -0.1, -1.75), .2f, Vec3(0.05, 0.11, 0.23), MaterialType::REFL_AND_REFR);
	s3->getMaterial().setShininess(120.f);
	s3->getMaterial().setSpecular(.6f);
	s3->getMaterial().setIOR(1.5f);
	s3->getMaterial().setReflection(1.0f);
	s3->getMaterial().setTransparent(1.0f);
	rt.addObject(s3);

	rt.addObject(new Plane(Vec3(1, 0, 0), 1.25, Vec3(0.75, .25, .25), MaterialType::DIFF)); // Left
	rt.addObject(new Plane(Vec3(-1, 0, 0), 1.25, Vec3(0.25, 0.25, 0.75), MaterialType::DIFF)); // Right
	rt.addObject(new Plane(Vec3(0, 1, 0), .75, Vec3(0.75), MaterialType::DIFF)); // Bottom
	rt.addObject(new Plane(Vec3(0, -1, 0), 1.25, Vec3(0.75), MaterialType::DIFF)); // Top
	rt.addObject(new Plane(Vec3(0, 0, 1), 5, Vec3(0.5, 0.25, .125), MaterialType::DIFF)); // Front
	rt.addObject(new Plane(Vec3(0, 0, -1), 5, Vec3(0.75), MaterialType::DIFF)); // Back

	Sphere* l1 = new Sphere(Vec3(0., 2.5f, -2.5), 2.5f, Vec3(1.0f, 1.0f, 1.0f), MaterialType::INVALID);
	Sphere* l2 = new Sphere(Vec3(0., 2.5f, -1.5), 5.f, Vec3(1.0f, 1.0f, 1.0f), MaterialType::INVALID);

	l1->getMaterial().setLightIntensity(1.5f);
	l2->getMaterial().setLightIntensity(1.75f);

	//rt.addLight(l1);
	rt.addLight(l2);

	printf("Scene initialized\n");

	auto timer = high_resolution_clock();

	auto begin = timer.now();
	printf("Starting to render...\n");
	rt.render();
	auto end = timer.now();
	auto diff = duration_cast<milliseconds> (end - begin).count();
	printf("Render time: %lld ms\n", diff);

	begin = timer.now();
	rt.export();
	end = timer.now();
	diff = duration_cast<milliseconds> (end - begin).count();
	printf("Export time: %lld ms\n", diff);

	system("pause");

	return 0;
}