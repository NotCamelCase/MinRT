### MinRT ###
MinRT is a minimal raytracer I wrote to give raytracing a try.

It simply features soft & hard shadows, diffuse and specular shading, perfect reflections and refractions with MSAA and gamma-correction support.

<img src="http://i.imgur.com/yb2wr3K.jpg" title="Screenshot showing one reflective, one diffuse and one glass-like object" />

Things I'd like to add soon:
* Procedural texturing
* Indirect lighting
* HDR
